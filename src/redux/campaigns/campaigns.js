import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit'
import axios from 'axios';

export const campaignsAdapter = createEntityAdapter({
  selectId: campaign => campaign.name
});

export const fetchCampaigns = createAsyncThunk(
  'campaigns/fetch',
  ({ page, perPage }) => axios({
    method: 'GET',
    url: 'https://dps-crowdsourcing-api-qa.azurewebsites.net/api/Campaign/open-projects',
    params: {
      pageIndex: page,
      pageSize: perPage,
      isGrid: true,
      sortField: "endDate",
      asc: false
    },
  }).then(response => ({ campaigns: response.data.result.items, count: response.data.result.total }))
);

const initialState = campaignsAdapter.getInitialState({
  loading: 'idle',
  count: 0,
  currentRequestId: undefined,
  error: null
});

export const campaigns = createSlice({
  name: 'campaigns',
  initialState,
  reducers: {},
  extraReducers: {
    [fetchCampaigns.pending]: (state, action) => {
        state.loading = 'pending';
        state.currentRequestId = action.meta.requestId;
        state.error = null
    },
    [fetchCampaigns.fulfilled]: (state, action) => {
      const { requestId } = action.meta;
      if(requestId === state.currentRequestId) {
        const { campaigns, count } = action.payload;
        state.loading = 'idle';
        campaignsAdapter.setAll(state, campaigns);
        state.count = count;
        state.currentRequestId = undefined;
      }
      
    },
    [fetchCampaigns.rejected]: (state, action) => {
      const { requestId } = action.meta;
      if(requestId === state.currentRequestId) {
        state.loading = 'idle';
        state.error = action.error.message;
        state.currentRequestId = undefined;
      }
    }
  }
})

export default campaigns.reducer;

