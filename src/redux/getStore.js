import { configureStore } from '@reduxjs/toolkit'
import pokemons from './pokemons/pokemons';
import pokemonDetail from './pokemonDetail/pokemonDetail';
import campaigns from './campaigns/campaigns';
import campaignDetail from './campaignDetail/campaignDetail';

export const getStore = (preloadedState) => configureStore({
  reducer: {
    pokemons,
    pokemonDetail,
    campaigns,
    campaignDetail,
  },
  preloadedState
});

export default getStore;
