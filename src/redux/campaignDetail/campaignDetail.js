import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import axios from 'axios';

export const fetchCampaignDetail = createAsyncThunk(
  'campaignDetail/fetch',
  ({ id }) => axios({
    method: 'GET',
    url: `https://dps-crowdsourcing-api-qa.azurewebsites.net/api/Campaign/get-active/${ id }`,
  }).then(response => ({ ...response.data.result }))
);

const initialState = {
  loading: 'idle',
  entity: null,
  currentRequestId: undefined,
  error: null
};

export const campaignDetail = createSlice({
  name: 'campaignDetail',
  initialState,
  reducers: {
  },
  extraReducers: {
    [fetchCampaignDetail.pending]: (state, action) => {
      state.loading = 'pending';
      state.currentRequestId = action.meta.requestId;
      state.error = null
    },
    [fetchCampaignDetail.fulfilled]: (state, action) => {
      const { requestId } = action.meta;
      if (requestId === state.currentRequestId) {
        state.entity = action.payload;
        state.loading = 'idle';
        state.currentRequestId = undefined;
      }
      
    },
    [fetchCampaignDetail.rejected]: (state, action) => {
      const { requestId } = action.meta;
      if (requestId === state.currentRequestId) {
        state.loading = 'idle';
        state.error = action.error.message;
        state.currentRequestId = undefined;
      }
    }
  }
})

export default campaignDetail.reducer;