import { pokemonsAdapter } from './pokemons/pokemons';
import { campaignsAdapter } from './campaigns/campaigns';

export const pokemonsSelectors = {
  selectCount: state => state.pokemons.count,
  selectLoading: state => state.pokemons.loading,
  selectAll: pokemonsAdapter.getSelectors( state => state.pokemons).selectAll
};

export const pokemonDetailSelectors = {
  selectEntity: state => state.pokemonDetail.entity,
  selectLoading: state => state.pokemonDetail.loading,
  selectError: state => state.pokemonDetail.error
};

export const campaignsSelectors = {
  selectCount: state => state.campaigns.count,
  selectLoading: state => state.campaigns.loading,
  selectAll: campaignsAdapter.getSelectors( state => state.campaigns).selectAll
}

export const campaignDetailSelectors = {
  selectEntity: state => state.campaignDetail.entity,
  selectLoading: state => state.campaignDetail.loading,
  selectError: state => state.campaignDetail.error
};