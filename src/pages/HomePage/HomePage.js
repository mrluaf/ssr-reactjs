import React from 'react';
import { Helmet } from 'react-helmet-async';
import { Typography, Container } from '@material-ui/core';
import CONFIGS from "../../consts/CONFIGS";

const HomePage = () => {
  return (
    <Container style={{marginTop: "20px"}}>
      <Helmet>
        <meta charSet="utf-8" />
        <title>SSR cho ReactJS - Nguyễn Văn Tài</title>
        <link rel="canonical" href={`${CONFIGS.ROOT_URL}`} />
        <meta name="description" content="Đây là website được code bằng ReactJS nhưng được cấu hình SSR bằng NodeJS. Có cấu hình một số thẻ Meta để tối ưu SEO" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="author" content="Nguyễn Văn Tài" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@nguyenvantai_vn" />
        <meta property="og:title" content="SSR cho ReactJS - Nguyễn Văn Tài" />
        <meta property="og:description" content="Đây là website được code bằng ReactJS nhưng được cấu hình SSR bằng NodeJS. Có cấu hình một số thẻ Meta để tối ưu SEO" />
        <meta property="og:type" content="article" />
        <meta property="og:url" content={`${CONFIGS.ROOT_URL}`} />
        <meta property="og:image" content="https://nguyenvantai.vn/wp-content/uploads/2022/03/nguyenvantai-avatar-550x825.jpg" />
        <meta property="og:image:width" content="200" />
        <meta property="og:image:height" content="200" />
        <meta property="og:site_name" content="SSR React - Nguyễn Văn Tài" />
      </Helmet>
      <div>
      <Typography variant="h4">Home page</Typography>
      <Typography>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non eros eu velit consectetur feugiat.
        Suspendisse condimentum augue tortor, non faucibus massa sodales et. Donec at erat pretium, sagittis enim
        posuere, congue massa.
        Aliquam dapibus metus justo, quis fringilla metus consectetur a. Sed ornare volutpat nibh, euismod rutrum ante
        bibendum eget.
        Cras suscipit odio ac libero feugiat suscipit. Nullam purus urna, pharetra ac felis id, vehicula volutpat felis.
        Nullam a magna euismod, porttitor dolor placerat, viverra nibh. Aliquam sed accumsan eros, ac tempus erat.
        Praesent vehicula sapien nec arcu sodales, et condimentum mi elementum. In efficitur sapien vitae luctus
        volutpat.
        Suspendisse interdum rhoncus dolor nec tincidunt. Phasellus pellentesque ipsum non diam bibendum ullamcorper.
        Aliquam ultricies fringilla feugiat. Morbi ultricies sed dolor id accumsan.
      </Typography>
    </div>
    </Container>
  );
};

export default HomePage;
