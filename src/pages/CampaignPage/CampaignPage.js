import React from 'react';
import { useSelector } from 'react-redux';
import { campaignDetailSelectors } from '../../redux/selectors';
import { Typography, Container } from '@material-ui/core';
import { Helmet } from 'react-helmet-async';
import moment from 'moment';
import 'moment-timezone';
import CONFIGS from "../../consts/CONFIGS";

const CampaignPage = () => {
  const campaign = useSelector(campaignDetailSelectors.selectEntity);
  const loading = useSelector(campaignDetailSelectors.selectLoading);
  const error = useSelector(campaignDetailSelectors.selectError);
  if (loading==='pending') return <h1>Loading</h1>;
  if (error) return <h3 style={ { color: 'red' } }> { error } </h3>
  if (campaign!==null) {
    return (
      <Container style={{marginTop: "20px"}}>
        <Helmet>
          <title>{ campaign.name }</title>
          <link rel="canonical" href={`${CONFIGS.ROOT_URL}/campaigns/${campaign.id}`} />
          <meta name="description" content={`${campaign.shortDescription}`} />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
          <meta name="author" content="Nguyễn Văn Tài" />
          <meta name="twitter:card" content="summary" />
          <meta name="twitter:site" content="@nguyenvantai_vn" />
          <meta property="og:title" content={`${campaign.name}`} />
          <meta property="og:description" content={`${campaign.shortDescription}`} />
          <meta property="og:type" content="article" />
          <meta property="og:url" content={`${CONFIGS.ROOT_URL}/campaigns/${campaign.id}`} />
          <meta property="og:image" content={ campaign.thumbnailUrl } />
          <meta property="og:image:width" content="200" />
          <meta property="og:image:height" content="200" />
          <meta property="og:site_name" content="Campaign detail SSR React - Nguyễn Văn Tài" />
        </Helmet>
        <Typography>ID: { campaign.id }</Typography>
        <Typography>Name: { campaign.name }</Typography>
        <Typography>Start Date: { moment(campaign.startDate).tz('Asia/Ho_Chi_Minh').format('DD/MM/YYYY') }</Typography>
        <Typography>End Date: { moment(campaign.endDate).tz('Asia/Ho_Chi_Minh').format('DD/MM/YYYY') }</Typography>
        <Typography>Short Description: { campaign.shortDescription }</Typography>
        <Typography>Description</Typography>
        <div style={{border: "solid", padding: "5px", marginBottom: "10px"}} dangerouslySetInnerHTML={{
          __html: campaign.description
        }}>
        </div>
        <img style={{border: "solid", padding: "5px", marginRight: "10px"}} src={ campaign.thumbnailUrl } alt="Thumbnail campaign" height="200"/>
        <img style={{border: "solid", padding: "5px"}} src={ campaign.bannerUrl } alt="Banner campaign" height="200"/>
      </Container>
    );
  }
  return null;
};

export default CampaignPage;
