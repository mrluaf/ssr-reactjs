const PATHS = {
  HOME: '/',
  POKEMONS: '/pokemons',
  POKEMON_DETAIL: '/pokemons/:id',
  CAMPAIGNS: '/campaigns',
  CAMPAIGN_DETAIL: '/campaigns/:id',
  ADMIN: '/admin',
  NOT_FOUND: '*'
}

export const PATHS_WITH_SSR = [
  PATHS.HOME,
  PATHS.POKEMONS,
  PATHS.POKEMON_DETAIL,
  PATHS.CAMPAIGNS,
  PATHS.CAMPAIGN_DETAIL,
];

export default PATHS;
